# Sick days schema

# --- !Ups

CREATE TABLE sick_day (
  id        			SERIAL PRIMARY KEY,
  employee_employee_number INTEGER,
  doctor_cert			BOOLEAN,
  start_date      DATE,
  end_date				DATE,
  vab					BOOLEAN,
  child_civic_reg_number	TEXT
);

INSERT INTO sick_day(employee_employee_number, doctor_cert, start_date, end_date, vab, child_civic_reg_number) VALUES (404,'f','2014-01-01','2014-01-01','f', '');
INSERT INTO sick_day(employee_employee_number, doctor_cert, start_date, end_date, vab, child_civic_reg_number) VALUES (404,'t','2014-01-02','2014-01-11','f', '');
INSERT INTO sick_day(employee_employee_number, doctor_cert, start_date, end_date, vab, child_civic_reg_number) VALUES (404,'f','2014-01-12','2014-01-12','t', '201401125122');

# --- !Downs

DROP TABLE IF EXISTS sick_day;

