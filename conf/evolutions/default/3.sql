# Logged actions schema

# --- !Ups

CREATE TABLE logged_action (
  id      SERIAL PRIMARY KEY,
  employee_id INTEGER,
  ip_address TEXT,
  created_at TIMESTAMP,
  action TEXT
);

# --- !Downs

DROP TABLE IF EXISTS logged_action;
