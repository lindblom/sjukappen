# Users schema

# --- !Ups

CREATE TABLE employee (
  employee_number		INTEGER PRIMARY KEY,
  password  			TEXT NOT NULL, 
  first_name				TEXT NOT NULL,
  last_name				TEXT NOT NULL,
  address				TEXT NOT NULL,
  zip_code				TEXT NOT NULL,
  city					TEXT NOT NULL,
  phone					TEXT,
  cell_phone				TEXT,
  job_phone				TEXT,
  email					TEXT,
  civic_reg_number		TEXT,
  last_failed_login   TIMESTAMP,
  admin         BOOLEAN NOT NULL DEFAULT 'f'
);


INSERT INTO employee VALUES (302, 'userpass', 'Allan', 'Lindblom', 'Norra Parkgatan 9', '524 30', 'Herrljunga', '', '0767946464', '051354638', 'hello@lindblom.co', '198405245575', NULL, 'f');
INSERT INTO employee VALUES (404, 'mittpass', 'Christopher', 'Lindblom', 'Norra Parkgatan 9', '524 30', 'Herrljunga', '', '0767946464', '051354638', 'hello@lindblom.co', '198405245575', NULL, 't');

# --- !Downs

DROP TABLE IF EXISTS employee;
