package controllers;

import java.util.Date;

import play.mvc.*;
import models.*;
import play.data.*;
import play.db.ebean.Transactional;

/**
 * Controller responsible for login and logout.
 * @author Gabriel Nilsson and Christopher Lindblom
 * @version 2014-03-14
 *
 */
public class Sessions extends Controller {	
	
	public final static String BAD_LOGIN = "Felaktiga inloggningsuppgifter!";
	public final static String WELCOME_BACK = "Välkommen tillbaka ";
	
	/**
	 * Attempts to login user with given credentials. If successful, the user will
	 * be redirected to the sick days form, otherwise the user will be redirected
	 * to the login page.
	 * @return html code for page the user was redirected to
	 */
	@Transactional
	public static Result loginSick() {
		Form<LoginCredentials> loginForm = play.data.Form.form(LoginCredentials.class);
		LoginCredentials loginCred = loginForm.bindFromRequest().get();
		Employee employee = fetchAuthenticatedEmployee(loginCred.username, loginCred.password);
		
		if(employee != null)
		{
			session("userID", employee.employeeNumber.toString());
			flash("success", WELCOME_BACK + employee.firstName);
			LoggedAction.log(employee.employeeNumber, "Loggade in", request().remoteAddress());
			return redirect(routes.SickDays.index()); 
		}
		
		flash("error", BAD_LOGIN);
		logFailedLoginAttempt(loginCred.username);
		return redirect(routes.Application.index());
    }
	
	/**
	 * Attempts to login user as admin with given credentials. If successful, the 
	 * user will be redirected to the form for searching employees, otherwise the 
	 * user will be redirected to the login page.
	 * @return html code for page the user was redirected to
	 */
	@Transactional
	public static Result loginCheckEmployees() {
		Form<LoginCredentials> loginForm = play.data.Form.form(LoginCredentials.class);
		LoginCredentials loginCred = loginForm.bindFromRequest().get();
		Employee employee = fetchAuthenticatedEmployee(loginCred.username, loginCred.password);
		
		if(employee != null && employee.admin)
		{
			session("userID", employee.employeeNumber.toString());
			session("admin", "userID");
			flash("success", WELCOME_BACK + employee.firstName);
			LoggedAction.log(employee.employeeNumber, "Loggade in som admin", request().remoteAddress());
			return redirect(routes.Employees.index());
		}
		
		flash("error", BAD_LOGIN);
		logFailedLoginAttempt(loginCred.username);
		return redirect(routes.Application.index());
    }
	
	/**
	 * Performs logout by clearing the session and redirecting the user to the
	 * start page (login page).
	 * @return html code for the start page
	 */
	public static Result logout() {
		if(session("userID") != null)
			LoggedAction.log(Integer.parseInt(session("userID")), "Loggade ut", request().remoteAddress());
		
		String flashMessage = flash("success");
		
		session().clear();
		
		if (flashMessage == null)
			flash("success", "Du har nu blivit utloggad");
		else
			flash("success", flashMessage);
		
		return redirect(routes.Application.index());
	}
	
	/**
	 * Returns the Employee object with the given credentials.
	 * @param employeeId - ID number for employee
	 * @param password - Password for employee
	 * @return Employee with given credentials, or null if credentials are incorrect
	 */
	private static Employee fetchAuthenticatedEmployee(String employeeId, String password) {
		Employee employee = null;
		
		try {
			employee = Employee.find.byId(Integer.parseInt(employeeId));
			
			if(!employee.isLoginable())
				employee = null;
			
		} catch (NumberFormatException | NullPointerException e) { }
		
		if(employee == null)
			return null;
		
		if(employee.checkPassword(password))
			return employee;
		
		employee.lastFailedLogin = new Date();
		employee.save();
		
		return null;
	}
	
	/**
	 * Logs a failed login attempt.
	 * @param employeeId - ID provided by the one attempting login
	 */
	private static void logFailedLoginAttempt(String employeeId) {
		LoggedAction.log(null, "Misslyckat inloggninsförsök som: " + employeeId, request().remoteAddress());
	}
}
