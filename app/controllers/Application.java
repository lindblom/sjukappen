package controllers;

import play.mvc.*;
import views.html.*;

/**
 * Entry point controller, responsible for rendering the index page.
 * @author Gabriel Nilsson and Christopher Lindblom
 * @version 2014-03-14
 *
 */
public class Application extends Controller {

	/**
	 * Renders the start page.
	 * @return html for the start page
	 */
    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

}
