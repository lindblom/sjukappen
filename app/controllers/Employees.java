package controllers;

import play.mvc.*;
import views.html.*;
import models.*;

/**
 * Controller responsible for searching and listing information about employees.
 * @author Gabriel Nilsson and Christopher Lindblom
 * @version 2014-03-14
 *
 */
@Security.Authenticated(SecuredAdminLogin.class)
public class Employees extends Controller {
	
	/**
	 * Renders the employees template.
	 * @return html for employees template
	 */
	public static Result index() {
		return ok(employees.render());
	}
	
	/**
	 * Gets information about the requested employee from the database, if available.
	 * @return html with employee info
	 */
	public static Result displayEmployee() {

		String requestedEmployeeNumber = request().getQueryString("employeeNumber");
		Employee employee = null;
		
		try {
			employee = Employee.find.byId(Integer.parseInt(requestedEmployeeNumber));
		} catch (NumberFormatException e) { }
		
		if(employee == null) {
			flash("error", "Den anställde du söker \"" + requestedEmployeeNumber + "\" finns inte.");
			LoggedAction.log(Integer.parseInt(session("userID")), "Sökte men fann inte anställd " + requestedEmployeeNumber, request().remoteAddress());
			return redirect(routes.Employees.index());
		}
		
		LoggedAction.log(Integer.parseInt(session("userID")), "Kollade på anställd " + employee.employeeNumber, request().remoteAddress());
		
		return ok(employeeinfo.render(employee));
	}
}
