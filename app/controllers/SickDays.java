package controllers;

import java.text.ParseException;
import java.util.Date;

import models.*;
import play.data.Form;
import play.data.validation.Constraints.Required;
import play.mvc.*;
import views.html.*;

/**
 * Controller responsible for rendering the sickdays template and registering
 * posted form information.
 * @author Gabriel Nilsson and Christopher Lindblom
 * @version 2014-03-16
 *
 */
@Security.Authenticated(SecuredBasicLogin.class)
public class SickDays extends Controller {
	
	final public static String SUCCESS_MESSAGE = "Din anmälan har registrerats och du har loggats ut!";
	final public static String VAB_LOG_MESSAGE = "Anmälde sig hemma med sjukt barn";
	final public static String DOC_LOG_MESSAGE = "Anmälde sig sjukskriven med läkarintyg";
	final public static String SICK_LOG_MESSAGE = "Anmälde sig sjuk.";
	
	/**
	 * Renders a page with a form for choice between sick leave and parental care
	 * of a sick child
	 * @return html for choice form
	 */
	public static Result index() {
		return ok(sickdaysentry.render());
	}
	
	/**
	 * Renders a page with a form for choice between sick leave with or without
	 * doctor's certificate.
	 * @return html with choice form
	 */
	public static Result sick() {
		return ok(sickdayssick.render());
	}
	
	/**
	 * Renders the page with the form for reporting sick leave with doctor's
	 * cerficate.
	 * @return html for sick leave page
	 */
	public static Result haveCertificate() {
		return ok(sickdayssicktime.render(new SickTime()));
	}
	
	/**
	 * Registers sick leave without doctor's certificate, reports success and
	 * performs logout on the user.
	 * @return html for startpage
	 */
	public static Result haveNoCertificate() {
		SickDay sickDay = new SickDay();
		sickDay.startDate = new Date();
		sickDay.endDate = new Date();
		sickDay.doctorCert = false;
		sickDay.vab = false;
		sickDay.childCivicRegNumber = null;
		
		return completeRegistration(sickDay, SICK_LOG_MESSAGE, request().remoteAddress());
	}
	
	/**
	 * Renders the form for registering parental care of a sick child.
	 * @return html code for page with parental care form
	 */
	public static Result vab() {
		return ok(sickdaysvab.render(new CivicRegNumber()));
	}
	
	/**
	 * Registers sick leave with doctor's certificate, with the given start and
	 * end date, reports success and performs logout on the user.
	 * @return html for the startpage or the form again if it isn't valid
	 */
	public static Result registerTime() {
		Form<SickTime> timeForm = play.data.Form.form(SickTime.class);
		SickTime sickTime = timeForm.bindFromRequest().get();
		
		if(sickTime.isValid()) {
			
			SickDay sickDay = new SickDay();
			try {
				sickDay.startDate = ValidatableForm.DATE_FORMAT.parse(sickTime.startDate);
				sickDay.endDate = ValidatableForm.DATE_FORMAT.parse(sickTime.endDate);
			} catch (ParseException e) {
				// already validated should not happen
			}
			
			sickDay.doctorCert = true;
			sickDay.vab = false;
			sickDay.childCivicRegNumber = null;
	
			return completeRegistration(sickDay, DOC_LOG_MESSAGE, request().remoteAddress());
		
		} else {
			
			return ok(sickdayssicktime.render(sickTime));
			
		}
	}
	
	/**
	 * Registers parental care of a sick child with the given civic registration
	 * number, reports success and perform logout on the user.
	 * @return html for startpage or the form again if it isn't valid
	 */
	public static Result registerVab() {
		Form<CivicRegNumber> vabForm = play.data.Form.form(CivicRegNumber.class);
		CivicRegNumber regNumber = vabForm.bindFromRequest().get();
		
		if(regNumber.isValid()) {
			SickDay sickDay = new SickDay();
			sickDay.startDate = new Date();
			sickDay.endDate = new Date();
			sickDay.doctorCert = false;
			sickDay.vab = true;
			sickDay.childCivicRegNumber = regNumber.civicRegNumber;
			
			return completeRegistration(sickDay, VAB_LOG_MESSAGE, request().remoteAddress());
		} else {
			return ok(sickdaysvab.render(regNumber));
		}
	}
	
	/**
	 * Completes the registration of sick leave or parental care of sick child.
	 * @param sickDay - SickDay object with information about the sick leave
	 * @param logMessage - Message about this action, meant for the log
	 * @param remoteAddress - User's ip adress
	 * @return html for startpage
	 */
	private static Result completeRegistration(SickDay sickDay, String logMessage, String remoteAddress) {
		// get the current user
		Employee employee = Employee.find.byId(Integer.parseInt(session("userID")));
		sickDay.employee = employee;
		
		sickDay.save();
		
		LoggedAction.log(Integer.parseInt(session("userID")), logMessage, remoteAddress);
		
		flash("success", SUCCESS_MESSAGE);
		return redirect(routes.Sessions.logout());
	}
}
