package controllers;

import play.mvc.Result;
import play.mvc.Security;
import play.mvc.Http.Context;

/**
 * Authenticator for ordinary users.
 * @author Gabriel Nilsson
 * @version 2014-03-15
 *
 */
public class SecuredBasicLogin extends Security.Authenticator {
	
	@Override
	public String getUsername(Context context) {
		return context.session().get("userID");
	}
	
	@Override
	public Result onUnauthorized(Context ctx) {
		return redirect(routes.Application.index());
	}

}
