package models;

/**
 * Very simple data class for a civic registration number.
 * @author Gabriel Nilsson
 * @version 2014-03-16
 *
 */
public class CivicRegNumber extends ValidatableForm {
	public String civicRegNumber;
	
	@Override
	public void validate() {
		civicRegNumberValidator(civicRegNumber, "civicRegNumber");
	}
	
	
}
