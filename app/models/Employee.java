package models;

/**
 * Data class with information about an employee.
 * 
 * @author Gabriel Nilsson and Christopher Lindblom
 * @version 2014-03-14
 */

import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.*;

import play.db.ebean.*;
import play.data.validation.*;
import play.data.validation.Constraints.Required;

@Entity
public class Employee extends Model {

	private static final String LAST_FAILED_LOGIN_DATE_FORMAT = "yyyy-mm-dd HH:mm:ss";
	private static final SimpleDateFormat LAST_FAILED_LOGIN_DATE_FORMATTER = new SimpleDateFormat(LAST_FAILED_LOGIN_DATE_FORMAT);
	
	private static final long serialVersionUID = 1L;

	@Id
	public Integer employeeNumber;
	
	@Required
	public String password;
	
	@Required
	public String firstName;
	
	@Required
	public String lastName;
	
	@Required
	public String address;
	
	@Required
	public String zipCode;
	
	@Required
	public String city;
	
	public String phone;
	public String cellPhone;
	public String jobPhone;
	
	@Constraints.Email
	public String email;
	
	public Date lastFailedLogin;
	
	public boolean admin;
	
	@OneToMany(mappedBy="employee")
	public List<SickDay> sickDays;
	
	@Required
	@Constraints.MaxLength(12)
	@Constraints.MinLength(12)
	public String civicRegNumber;
	
	/**
	 * Confirms that the given password is correct.
	 * @param password - Given password
	 * @return true if passwords match, otherwise false
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
	
	/**
	 * Checks if the user is allowed to log in at the given time or not. Only one
	 * attempt is allowed every second.
	 * @return true if login is allowed, false if not
	 */
	public boolean isLoginable() {
		if(lastFailedLogin == null)
			return true;
		
		String lastTime = LAST_FAILED_LOGIN_DATE_FORMATTER.format(lastFailedLogin);
		String now = LAST_FAILED_LOGIN_DATE_FORMATTER.format(new Date());
		return !lastTime.equalsIgnoreCase(now);
	}
	
	public static Finder<Integer, Employee> find = new Finder<Integer, Employee>(Integer.class, Employee.class);
}
