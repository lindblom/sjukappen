package models;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for form data classes to inherit from to become validatable
 * 
 * @author Christopher Lindblom
 * @version 2014-03-16
 */
public abstract class ValidatableForm {
	
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat CIVIC_DATE_PART_FORMAT = new SimpleDateFormat("yyyyMMdd");
	public Map<String, String> errors = new HashMap<>();
	
	/**
	 * Checks if the form is valid.
	 * @return true if valid, false if not
	 */
	public boolean isValid() {
		validate();
		
		return errors.isEmpty();
	}
	
	/**
	 * Validates the form.
	 */
	abstract void validate();
	
	/**
	 * Validates civic registration numbers.
	 * @param civicRegNumber - String to validate
	 * @param field - Name of form field to validate
	 */
	public void civicRegNumberValidator(String civicRegNumber, String field) {
		
		if(errors.containsKey(field))
			return;
		
		try {
			new BigDecimal(civicRegNumber);
			String datePart = civicRegNumber.substring(0, 8);
			Date date = CIVIC_DATE_PART_FORMAT.parse(datePart);
			
			if (civicRegNumber.length() != 12 || !CIVIC_DATE_PART_FORMAT.format(date).equals(datePart)) {
				throw new Exception();
			}
			
		} catch (Exception e) {
			errors.put(field, "Personnumret måste anges med 12 siffor utan specialtecken och de första åtta måste vara ett giltigt datum.");
		}
	}
	
	/**
	 * Validates dates.
	 * @param dateString - String to validate
	 * @param field - Name of form field to validate
	 */
	public void dateValidator(String dateString, String field) {
		
		if(errors.containsKey(field))
			return;
		
		try {
			Date date = DATE_FORMAT.parse(dateString);
			String dateAfterString = DATE_FORMAT.format(date);
			
			if(!dateAfterString.equals(dateString))
				throw new Exception();
			
		} catch (Exception e) {
			errors.put(field, "Måste vara ett giltigt datum med formatet (YYYY-MM-DD)");
		}
	}
	
	/**
	 * Confirms that the first date isn't later than the second.
	 * @param firstDateString - String with the date that should be first
	 * @param secondDateString - String with the date that should be second
	 * @param field
	 */
	public void dateBeforeOrSameDateValidator(String firstDateString, String secondDateString, String field) {
		
		if(errors.containsKey(field))
			return;
		
		try {
			Date firstDate = DATE_FORMAT.parse(firstDateString);
			Date secondDate = DATE_FORMAT.parse(secondDateString);
			
			if(firstDate.compareTo(secondDate) > 0)
				errors.put(field, "Får inte vara före " + firstDateString);
			
		} catch (Exception e) { }
		
	}
}
