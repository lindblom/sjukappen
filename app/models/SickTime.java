package models;

/**
 * Data class for the duration of a sickness registration.
 * @author Gabriel Nilsson och Christopher Lindblom
 * @version 2014-03-16
 *
 */
public class SickTime extends ValidatableForm {
	public String startDate;
	public String endDate;
	
	@Override
	void validate() {
		dateValidator(startDate, "startDate");
		dateValidator(endDate, "endDate");
		dateBeforeOrSameDateValidator(startDate, endDate, "endDate");
	}
}
