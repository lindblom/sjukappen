package models;

import java.util.*;

import javax.persistence.*;

import play.data.validation.Constraints.Required;
import play.db.ebean.*;

@Entity
public class SickDay extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sick_day_id_seq")
	public long id;

	@Required
	public Date startDate = new Date();
	
	@Required
	public Date endDate = new Date();
	
	public boolean doctorCert;
	public boolean vab;
	public String childCivicRegNumber;
	
	@ManyToOne
	public Employee employee;
	
	public static Finder<Long, SickDay> find = new Finder<Long, SickDay>(Long.class, SickDay.class);
}
