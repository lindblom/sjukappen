package models;

/**
 * Data class with login credentials.
 * @author Gabriel Nilsson
 * @version 2014-03-14
 *
 */

public class LoginCredentials {
	public String username;
	public String password;
}
