package models;


/**
* Data entity class with information about a logged action.
* 
* @author Christopher Lindblom
* @version 2014-03-15
*/

import java.util.*;

import javax.persistence.*;

import play.db.ebean.*;

@Entity
public class LoggedAction extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "logged_action_id_seq")
	public long id;
	
	public Integer employeeId;
	public String ipAddress;
	public Date createdAt;
	public String action;
	
	public static Finder<Long, LoggedAction> find = new Finder<Long, LoggedAction>(Long.class, LoggedAction.class);

	/**
	 * Logs a user action.
	 * @param employeeNumber - ID used by the user (or given by the user in failed login attempt)
	 * @param message - Description of the user action
	 * @param ip - User's ip adress
	 */
	public static void log(Integer employeeNumber, String message, String ip) {
		LoggedAction action = new LoggedAction();
		action.action = message;
		action.createdAt = new Date();
		action.ipAddress = ip;
		action.employeeId = employeeNumber;
		action.save();
	}
}
