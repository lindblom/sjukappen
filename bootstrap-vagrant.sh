#!/usr/bin/env bash

sudo add-apt-repository ppa:chris-lea/postgresql-9.3

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install -y postgresql-9.3

sudo -u postgres createuser sjukappen
sudo -u postgres psql -c "alter user sjukappen with password 'sjukappen'"
sudo -u postgres createdb -O sjukappen sjukappen

echo "host     all     all     samenet    md5" | sudo tee -a /etc/postgresql/9.3/main/pg_hba.conf

sed "s/^#listen_addresses.*/listen_addresses = '*'/" /etc/postgresql/9.3/main/postgresql.conf | sudo tee /etc/postgresql/9.3/main/postgresql.conf

sudo service postgresql restart
